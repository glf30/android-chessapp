package chess.cs213;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by Jean on 4/23/2018.
 */

public class ChessActivity extends AppCompatActivity {


    public GameBoard board;
    public Button undo1;
    public Button undo2;
    public Button resign1;
    public Button resign2;
    public Button ai1;
    public Button ai2;
    public Button menu;
    public MovePiece movePiece;
    public Thread t;
    public Game gameToRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chess);
        gameToRecord = new Game("temp");
        board = new GameBoard(ChessActivity.this, this);
        movePiece = new MovePiece(ChessActivity.this,this, gameToRecord);
        GameBoard.whiteMove = true;
        GameBoard.gameOver = false;
        board.fillBoard(movePiece);
        board.setBackground();
        board.gameIsRunning = true;



        undo1 = findViewById(R.id.undo1);
        undo1.setOnClickListener(movePiece);
        undo2 = findViewById(R.id.undo2);
        undo2.setOnClickListener(movePiece);

        resign1 = findViewById(R.id.resign1);
        resign1.setOnClickListener(movePiece);
        resign2 = findViewById(R.id.resign2);
        resign2.setOnClickListener(movePiece);

        ai1 = findViewById(R.id.ai1);
        ai1.setOnClickListener(movePiece);
        ai2 = findViewById(R.id.ai2);
        ai2.setOnClickListener(movePiece);

        menu = findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                board.gameIsRunning = false;
                Intent mainIntent = new Intent("chess.cs213.MAIN");
                startActivity(mainIntent);
            }
        });

        t = new DrawThread(board,this, gameToRecord);
        t.start();


    }



    @Override
    protected void onPause(){
        super.onPause();
    }

}

class DrawThread extends Thread
{
    GameBoard board;
    Activity activity;
    Game gameToRecord;
    DrawThread(GameBoard board, Activity activity, Game gameToRecord)
    {
        this.board = board;
        this.activity = activity;
        this.gameToRecord = gameToRecord;
    }
    public void run()
    {
        while(board.gameIsRunning)
        {
            if(board.isDraw())
            {
                board.endGame(activity, gameToRecord);
                GameBoard.gameOver = true;
                break;
            }
        }
    }

}
