package chess.cs213;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayDeque;
import java.util.Stack;

/**
 * Created by Jean on 4/24/2018.
 */

public class ChessReplayActivity  extends AppCompatActivity {

    private MovePiece movePiece;
    private GameBoard gameBoard;
    private Button menu;
    private Button next;
    private Button prev;
    private Game game;
    private int position = 0;
    private Move currentMove;
    private Stack<PieceCell[][]> prevBoardStack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chessreplay);
        game = (Game) getIntent().getSerializableExtra("GameObject");
        gameBoard = new GameBoard(ChessReplayActivity.this,this);
        movePiece = new MovePiece(ChessReplayActivity.this,this);
        prevBoardStack = new Stack<>();
        gameBoard.fillBoard(movePiece);
        gameBoard.setBackground();

        menu = findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mainIntent = new Intent("chess.cs213.MAIN");
                startActivity(mainIntent);
            }
        });


        next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(game.moveList.size() > position ) {
                    PieceCell[][] prevBoard = gameBoard.makePrevBoard();
                    prevBoardStack.push(prevBoard);
                    currentMove = game.moveList.get(position);
                    PieceCell currentPiece = (PieceCell) GameBoard.board[currentMove.fromX][currentMove.fromY].getTag();
                    int imageResource = currentPiece.getImageResource();
                    GameBoard.board[currentMove.fromX][currentMove.fromY].setImageResource(R.drawable.blank);
                    GameBoard.board[currentMove.fromX][currentMove.fromY].setTag(new PieceCell(currentMove.fromX,currentMove.fromX,R.drawable.blank));
                    GameBoard.board[currentMove.toX][currentMove.toY].setImageResource(imageResource);
                    GameBoard.board[currentMove.toX][currentMove.toY].setTag(new PieceCell(currentMove.toX,currentMove.toX,imageResource));
                    position++;
                }
            }
        });

        prev = findViewById(R.id.previous);
        prev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(!prevBoardStack.empty()) {
                    gameBoard.undo(gameBoard, prevBoardStack);
                    gameBoard.setBackground();
                    position--;
                }
            }
        });
    }
}
