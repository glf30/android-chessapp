package chess.cs213;

import android.widget.ImageView;

/**
 * Created by Jean on 4/18/2018.
 */

public class CurrentPiece {

    private int x;
    private int y;
    private ImageView piece;
    private boolean isSelected;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public ImageView getPiece() {
        return piece;
    }

    public void setPiece(ImageView piece) {
        this.piece = piece;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }



}
