package chess.cs213;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Jean on 4/26/2018.
 */

public class CustomListViewAdapterNameAndDate extends BaseAdapter {

    private ArrayList<Game> listData;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListViewAdapterNameAndDate(Context context, ArrayList<Game> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        //      int type = getItemViewType(position);

        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.activity_replaytest, null);
            holder = new ViewHolder();

            holder.name = (TextView) convertView.findViewById(R.id.nameTextView);
            holder.age = (TextView) convertView.findViewById(R.id.dateTextView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(listData.get(position).getName());
        holder.age.setText(listData.get(position).getDate());

        return convertView;
    }

    public void remove(Object item) {
        listData.remove(item);
    }

    static class ViewHolder {
        TextView name;
        TextView age;
    }

}