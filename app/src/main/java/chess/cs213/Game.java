package chess.cs213;

import android.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Jean on 4/24/2018.
 */

public class Game implements Serializable {

    public String name;
    public ArrayList<Move> moveList;
    private static final long serialVersionUID = 2L;
    Date date = new Date();

    public Game(String name) {
        this.name = name;
        moveList = new ArrayList<>();
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDate()
    {
        return new SimpleDateFormat("MM-dd-yyyy").format(date);
    }

    public String getName()
    {
        return name.toString();
    }

    public void add(Move move)
    {
        if(moveList == null)
        {
            Log.i("Null","This is a null object");
        }
        moveList.add(move);
    }

    @Override
    public String toString() {
        return name;
    }
}
