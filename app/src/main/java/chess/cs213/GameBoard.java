package chess.cs213;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.view.*;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Stack;

import chess.pieces.Bishop;
import chess.pieces.BlackPawn;
import chess.pieces.King;
import chess.pieces.Knight;
import chess.pieces.Queen;
import chess.pieces.Rook;
import chess.pieces.WhitePawn;

/**
 * Created by Jean on 4/14/2018.
 */

public class GameBoard{


    public static ImageView[][] board = new ImageView[8][8];
    public static CurrentPiece currentPiece = new CurrentPiece();
    private Activity activity;
    private Context context;
    public static int fromX = 0;
    public static int fromY = 0;
    public static boolean whiteMove = true;
    public static boolean isDrawWhite = false;
    public static boolean isDrawBlack = false;
    public static boolean gameOver = false;
    public static Stack<PieceCell[][]> prevBoard = new Stack<PieceCell[][]>();
    public static PieceCell[][][] prevBoard2 = new PieceCell[8][8][1];
    public boolean whiteWon = false;
    public boolean blackWon = false;
    private String fileName = "";
    public boolean gameIsRunning;
    public static boolean isQueen = true;
    public static boolean isPromotion = false;
    public static boolean whiteEPOnBlack = false;
    public static boolean blackEPOnWhite = false;
    public static int lastEPX = 0;
    public static int lastEPY = 0;

    public GameBoard(Context context, Activity activity)
    {
        this.context = context;
        this.activity = activity;
    }

    public boolean isDraw()
    {
        ToggleButton draw1 = activity.findViewById(R.id.draw1);
        ToggleButton draw2 = activity.findViewById(R.id.draw2);
        if(whiteMove == false) {
            draw1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        isDrawBlack = true;
                    } else {
                        isDrawBlack = false;
                    }
                }
            });
        }
        else {
            draw2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        isDrawWhite = true;
                    } else {
                        isDrawWhite = false;
                    }
                }
            });
        }
        if(isDrawWhite == true && isDrawBlack  == true)
            return true;
        else return false;
    }

    protected static boolean isWhite(Integer whiteTag){
        switch(whiteTag) {
            case R.drawable.whitequeen:
                return true;

            case R.drawable.whitebishop:
                return true;

            case R.drawable.whiteking:
                return true;

            case R.drawable.whitepawn:
                return true;

            case R.drawable.whiteknight:
                return true;

            case R.drawable.whiterook:
                return true;

            default:
                return false;
        }
    }

    protected void undo(GameBoard board)
    {
        PieceCell[][][] tempBoard = prevBoard2;
        for(int x = 0; x<8;x++ )
        {
            for(int y =0; y<8;y++)
            {
                board.board[x][y].setImageResource(tempBoard[x][y][0].getImageResource());
                board.board[x][y].setTag(tempBoard[x][y][0]);

            }
        }
    }


    public void undo(GameBoard gameBoard, Stack<PieceCell[][]> prevBoardStack) {
        PieceCell[][] tempBoard = prevBoardStack.pop();
        for(int x = 0; x<8;x++ )
        {
            for(int y =0; y<8;y++)
            {
                gameBoard.board[x][y].setImageResource(tempBoard[x][y].getImageResource());
                gameBoard.board[x][y].setTag(tempBoard[x][y]);

            }
        }
    }

    protected void fillBoard(View.OnClickListener listener) {
        board[0][7] = activity.findViewById(R.id.imageView01);
        board[0][7].setTag(new PieceCell(0,7,R.drawable.blackrook));
        board[1][7] =  activity.findViewById(R.id.imageView02);
        board[1][7].setTag(new PieceCell(1,7,R.drawable.blackknight));
        board[2][7] =  activity.findViewById(R.id.imageView03);
        board[2][7].setTag(new PieceCell(2,7,R.drawable.blackbishop));
        board[3][7] =  activity.findViewById(R.id.imageView04);
        board[3][7].setTag(new PieceCell(3,7,R.drawable.blackqueen));
        board[4][7] =  activity.findViewById(R.id.imageView05);
        board[4][7].setTag(new PieceCell(4,7,R.drawable.blackking));
        board[5][7] =  activity.findViewById(R.id.imageView06);
        board[5][7].setTag(new PieceCell(5,7,R.drawable.blackbishop));
        board[6][7] =  activity.findViewById(R.id.imageView07);
        board[6][7].setTag(new PieceCell(6,7,R.drawable.blackknight));
        board[7][7] =  activity.findViewById(R.id.imageView08);
        board[7][7].setTag(new PieceCell(7,7,R.drawable.blackrook));
        board[0][6] =  activity.findViewById(R.id.imageView09);
        board[0][6].setTag(new PieceCell(0,6,R.drawable.blackpawn));
        board[1][6] =  activity.findViewById(R.id.imageView10);
        board[1][6].setTag(new PieceCell(1,6,R.drawable.blackpawn));
        board[2][6] =  activity.findViewById(R.id.imageView11);
        board[2][6].setTag(new PieceCell(2,6,R.drawable.blackpawn));
        board[3][6] =  activity.findViewById(R.id.imageView12);
        board[3][6].setTag(new PieceCell(3,6,R.drawable.blackpawn));
        board[4][6] =  activity.findViewById(R.id.imageView13);
        board[4][6].setTag(new PieceCell(4,6,R.drawable.blackpawn));
        board[5][6] =  activity.findViewById(R.id.imageView14);
        board[5][6].setTag(new PieceCell(5,6,R.drawable.blackpawn));
        board[6][6] =  activity.findViewById(R.id.imageView15);
        board[6][6].setTag(new PieceCell(6,6,R.drawable.blackpawn));
        board[7][6] =  activity.findViewById(R.id.imageView16);
        board[7][6].setTag(new PieceCell(7,6,R.drawable.blackpawn));
        board[0][5] =  activity.findViewById(R.id.imageView17);
        board[0][5].setTag(new PieceCell(0,5,R.drawable.blank));
        board[1][5] =  activity.findViewById(R.id.imageView18);
        board[1][5].setTag(new PieceCell(1,5,R.drawable.blank));
        board[2][5] =  activity.findViewById(R.id.imageView19);
        board[2][5].setTag(new PieceCell(2,5,R.drawable.blank));
        board[3][5] =  activity.findViewById(R.id.imageView20);
        board[3][5].setTag(new PieceCell(3,5,R.drawable.blank));
        board[4][5] =  activity.findViewById(R.id.imageView21);
        board[4][5].setTag(new PieceCell(4,5,R.drawable.blank));
        board[5][5] =  activity.findViewById(R.id.imageView22);
        board[5][5].setTag(new PieceCell(5,5,R.drawable.blank));
        board[6][5] =  activity.findViewById(R.id.imageView23);
        board[6][5].setTag(new PieceCell(6,5,R.drawable.blank));
        board[7][5] =  activity.findViewById(R.id.imageView24);
        board[7][5].setTag(new PieceCell(7,5,R.drawable.blank));
        board[0][4] =  activity.findViewById(R.id.imageView25);
        board[0][4].setTag(new PieceCell(0,4,R.drawable.blank));
        board[1][4] =  activity.findViewById(R.id.imageView26);
        board[1][4].setTag(new PieceCell(1,4,R.drawable.blank));
        board[2][4] =  activity.findViewById(R.id.imageView27);
        board[2][4].setTag(new PieceCell(2,4,R.drawable.blank));
        board[3][4] =  activity.findViewById(R.id.imageView28);
        board[3][4].setTag(new PieceCell(3,4,R.drawable.blank));
        board[4][4] =  activity.findViewById(R.id.imageView29);
        board[4][4].setTag(new PieceCell(4,4,R.drawable.blank));
        board[5][4] =  activity.findViewById(R.id.imageView30);
        board[5][4].setTag(new PieceCell(5,4,R.drawable.blank));
        board[6][4] =  activity.findViewById(R.id.imageView31);
        board[6][4].setTag(new PieceCell(6,4,R.drawable.blank));
        board[7][4] =  activity.findViewById(R.id.imageView32);
        board[7][4].setTag(new PieceCell(7,4,R.drawable.blank));
        board[0][3] =  activity.findViewById(R.id.imageView33);
        board[0][3].setTag(new PieceCell(0,3,R.drawable.blank));
        board[1][3] =  activity.findViewById(R.id.imageView34);
        board[1][3].setTag(new PieceCell(1,3,R.drawable.blank));
        board[2][3] =  activity.findViewById(R.id.imageView35);
        board[2][3].setTag(new PieceCell(2,3,R.drawable.blank));
        board[3][3] =  activity.findViewById(R.id.imageView36);
        board[3][3].setTag(new PieceCell(3,3,R.drawable.blank));
        board[4][3] =  activity.findViewById(R.id.imageView37);
        board[4][3].setTag(new PieceCell(4,3,R.drawable.blank));
        board[5][3] =  activity.findViewById(R.id.imageView38);
        board[5][3].setTag(new PieceCell(5,3,R.drawable.blank));
        board[6][3] =  activity.findViewById(R.id.imageView39);
        board[6][3].setTag(new PieceCell(6,3,R.drawable.blank));
        board[7][3] =  activity.findViewById(R.id.imageView40);
        board[7][3].setTag(new PieceCell(7,3,R.drawable.blank));
        board[0][2] =  activity.findViewById(R.id.imageView41);
        board[0][2].setTag(new PieceCell(0,2,R.drawable.blank));
        board[1][2] =  activity.findViewById(R.id.imageView42);
        board[1][2].setTag(new PieceCell(1,2,R.drawable.blank));
        board[2][2] =  activity.findViewById(R.id.imageView43);
        board[2][2].setTag(new PieceCell(2,2,R.drawable.blank));
        board[3][2] =  activity.findViewById(R.id.imageView44);
        board[3][2].setTag(new PieceCell(3,2,R.drawable.blank));
        board[4][2] =  activity.findViewById(R.id.imageView45);
        board[4][2].setTag(new PieceCell(4,2,R.drawable.blank));
        board[5][2] =  activity.findViewById(R.id.imageView46);
        board[5][2].setTag(new PieceCell(5,2,R.drawable.blank));
        board[6][2] =  activity.findViewById(R.id.imageView47);
        board[6][2].setTag(new PieceCell(6,2,R.drawable.blank));
        board[7][2] =  activity.findViewById(R.id.imageView48);
        board[7][2].setTag(new PieceCell(7,2,R.drawable.blank));
        board[0][1] =  activity.findViewById(R.id.imageView49);
        board[0][1].setTag(new PieceCell(0,1,R.drawable.whitepawn));
        board[1][1] =  activity.findViewById(R.id.imageView50);
        board[1][1].setTag(new PieceCell(1,1,R.drawable.whitepawn));
        board[2][1] =  activity.findViewById(R.id.imageView51);
        board[2][1].setTag(new PieceCell(2,1,R.drawable.whitepawn));
        board[3][1] =  activity.findViewById(R.id.imageView52);
        board[3][1].setTag(new PieceCell(3,1,R.drawable.whitepawn));
        board[4][1] =  activity.findViewById(R.id.imageView53);
        board[4][1].setTag(new PieceCell(4,1,R.drawable.whitepawn));
        board[5][1] =  activity.findViewById(R.id.imageView54);
        board[5][1].setTag(new PieceCell(5,1,R.drawable.whitepawn));
        board[6][1] =  activity.findViewById(R.id.imageView55);
        board[6][1].setTag(new PieceCell(6,1,R.drawable.whitepawn));
        board[7][1] =  activity.findViewById(R.id.imageView56);
        board[7][1].setTag(new PieceCell(7,1,R.drawable.whitepawn));
        board[0][0] =  activity.findViewById(R.id.imageView57);
        board[0][0].setTag(new PieceCell(0,0,R.drawable.whiterook));
        board[1][0] =  activity.findViewById(R.id.imageView58);
        board[1][0].setTag(new PieceCell(1,0,R.drawable.whiteknight));
        board[2][0] =  activity.findViewById(R.id.imageView59);
        board[2][0].setTag(new PieceCell(2,0,R.drawable.whitebishop));
        board[3][0] =  activity.findViewById(R.id.imageView60);
        board[3][0].setTag(new PieceCell(3,0,R.drawable.whitequeen));
        board[4][0] =  activity.findViewById(R.id.imageView61);
        board[4][0].setTag(new PieceCell(4,0,R.drawable.whiteking));
        board[5][0] =  activity.findViewById(R.id.imageView62);
        board[5][0].setTag(new PieceCell(5,0,R.drawable.whitebishop));
        board[6][0] =  activity.findViewById(R.id.imageView63);
        board[6][0].setTag(new PieceCell(6,0,R.drawable.whiteknight));
        board[7][0] =  activity.findViewById(R.id.imageView64);
        board[7][0].setTag(new PieceCell(7,0,R.drawable.whiterook));

        for (int file = 0; file <= 7; file++) {
            for (int rank = 0; rank <= 7; rank++) {
                if (listener!=null) {
                    board[file][rank].setOnClickListener(listener);
                }
            }
        }

    }



    protected void setBackground() {
        for (int file = 0; file < 8; file++) {
            for (int rank = 0; rank < 8; rank++) {
                if (((file + rank) % 2) == 0) {
                    board[file][rank].setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
                } else {
                    board[file][rank].setBackgroundColor(ContextCompat.getColor(context,R.color.colorPrimary));
                }
            }

        }
    }

    public void endGame(final Activity activity, final Game gameToRecord) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if(isDraw())
                {
                    Toast.makeText(context, "Draw",Toast.LENGTH_SHORT).show();
                }
                else if(whiteWon)
                {
                    Toast.makeText(context, "White wins",Toast.LENGTH_SHORT).show();
                }
                else if(blackWon)
                {
                    Toast.makeText(context, "Black wins",Toast.LENGTH_SHORT).show();
                }

                AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                alert.setTitle("Save the game?");

                final EditText input = new EditText(activity);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                alert.setView(input);

                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fileName = input.getText().toString();
                        gameToRecord.setName(fileName);
                        GameList.add(gameToRecord);
                        GameList.saveToDAT();
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alert.show();
            }
        });
    }


    public static PieceCell[][] makePrevBoard() {
        PieceCell[][] previousCell = new PieceCell[8][8];
        PieceCell pieceCell;
        for(int x = 0; x < 8; x++)
        {
            for(int y = 0; y < 8; y++)
            {
                previousCell[x][y] =  (PieceCell) board[x][y].getTag();
            }
        }
        return previousCell;
    }
    public static boolean Check(boolean whitePutBlackInCheck) {
        int toX = 0;
        int toY = 0;

        //Piece tempPiece = new Piece();
        //go through board and find king
        //go through board and test valid movement for each piece to king
        if(whitePutBlackInCheck) {

            int r = 8;
            for(int y = 0; y<8; y++) {
                for(int x = 0; x<8; x++) {
                    PieceCell pieceTag = (PieceCell) GameBoard.board[x][y].getTag();
                    if(pieceTag.getImageResource()==R.drawable.blackking) {
                        toX = x;
                        toY = y;

                    }


                }

                r--;

            }

            r = 8;
            for(int y = 0; y<8; y++) {
                for(int x = 0; x<8; x++) {
                    //Integer pieceTag = (Integer) GameBoard.board[x][y].getTag();
                    PieceCell pieceTag = (PieceCell) GameBoard.board[x][y].getTag();

                    if(isWhite(pieceTag.imageResource) && pieceTag.imageResource!=(R.drawable.blank)) {

                        switch(pieceTag.imageResource) {
                            case R.drawable.whitequeen:

                                if(Queen.validMovement(x,y,toX,toY)){

                                    return true;
                                }

                                break;
                            case R.drawable.whitebishop:
                                if(Bishop.validMovement(x,y,toX,toY)){

                                    return true;
                                }
                                break;

                            case R.drawable.whiteking:
                                if(King.validMovement(x,y,toX,toY)){

                                    return true;
                                }
                                break;
                            case R.drawable.whitepawn:
                                if(WhitePawn.validMovement(x,y,toX,toY)){

                                    return true;
                                }
                                break;
                            case R.drawable.whiteknight:
                                if(Knight.validMovement(x,y,toX,toY)){

                                    return true;
                                }
                                break;
                            case R.drawable.whiterook:
                                if(Rook.validMovement(x,y,toX,toY)){

                                    return true;
                                }
                                break;

                            default:
                                //return false;
                                break;
                        }

                    }


                }

                r--;

            }




        } else if(!whitePutBlackInCheck) {
            int r = 8;
            for(int y = 0; y<8; y++) {
                for(int x = 0; x<8; x++) {
                    PieceCell pieceTag = (PieceCell) GameBoard.board[x][y].getTag();
                    if(pieceTag.getImageResource()==R.drawable.whiteking) {
                        toX = x;
                        toY = y;
                    }


                }

                r--;

            }

            r = 8;
            for(int y = 0; y<8; y++) {
                for(int x = 0; x<8; x++) {
                    PieceCell pieceTag = (PieceCell) GameBoard.board[x][y].getTag();
                    if(!isWhite(pieceTag.imageResource) && pieceTag.imageResource!=(R.drawable.blank)) {

                        switch(pieceTag.imageResource) {
                            case R.drawable.blackqueen:
                                if(Queen.validMovement(x,y,toX,toY)){
                                    return true;
                                }

                                break;
                            case R.drawable.blackbishop:
                                if(Bishop.validMovement(x,y,toX,toY)){
                                    return true;
                                }
                                break;

                            case R.drawable.blackking:
                                if(King.validMovement(x,y,toX,toY)){
                                    return true;
                                }
                                break;
                            case R.drawable.blackpawn:
                                if(BlackPawn.validMovement(x,y,toX,toY)){
                                    return true;
                                }
                                break;
                            case R.drawable.blackknight:
                                if(Knight.validMovement(x,y,toX,toY)){
                                    return true;
                                }
                                break;
                            case R.drawable.blackrook:
                                if(Rook.validMovement(x,y,toX,toY)){
                                    return true;
                                }
                                break;

                            default:
                                //return false;
                                break;
                        }

                    }


                }

                r--;

            }

        }


        return false;
    }



    public static ArrayList<int[]> getAllWhitePieces()
    {
        ArrayList<int[]> list = new ArrayList<int[]>();
        for(int x = 0; x < 8; x++)
        {
            for(int y = 0; y < 8; y++)
            {
                PieceCell pieceTag = (PieceCell) GameBoard.board[x][y].getTag();
                int[] toAdd = new int[]{};
                switch(pieceTag.getImageResource()) {


                    case R.drawable.whitequeen:
                        toAdd = new int[]{R.drawable.whitequeen,x,y};
                        list.add(toAdd);
                        break;
                    case R.drawable.whitebishop:
                        toAdd = new int[]{R.drawable.whitebishop,x,y};
                        list.add(toAdd);
                        break;

                    case R.drawable.whiteking:
                        toAdd = new int[]{R.drawable.whiteking,x,y};
                        list.add(toAdd);
                        break;


                    case R.drawable.whitepawn:
                        toAdd = new int[]{R.drawable.whitepawn,x,y};
                        list.add(toAdd);
                        break;

                    case R.drawable.whiteknight:
                        toAdd = new int[]{R.drawable.whiteknight,x,y};
                        list.add(toAdd);
                        break;

                    case R.drawable.whiterook:
                        toAdd = new int[]{R.drawable.whiterook,x,y};
                        list.add(toAdd);
                        break;
                    default:
                        break;
                }
            }
        }
        return list;
    }

    public static ArrayList<int[]> getAllBlackPieces()
    {
        ArrayList<int[]> list = new ArrayList<int[]>();
        for(int x = 0; x < 8; x++)
        {
            for(int y = 0; y < 8; y++)
            {

                PieceCell pieceTag = (PieceCell) GameBoard.board[x][y].getTag();
                int[] toAdd = new int[]{};
                switch(pieceTag.getImageResource()) {


                    case R.drawable.blackqueen:
                        toAdd = new int[]{R.drawable.blackqueen,x,y};
                        list.add(toAdd);
                        break;
                    case R.drawable.blackbishop:
                        toAdd = new int[]{R.drawable.blackbishop,x,y};
                        list.add(toAdd);
                        break;

                    case R.drawable.blackking:
                        toAdd = new int[]{R.drawable.blackking,x,y};
                        list.add(toAdd);
                        break;


                    case R.drawable.blackpawn:
                        toAdd = new int[]{R.drawable.blackpawn,x,y};
                        list.add(toAdd);
                        break;

                    case R.drawable.blackknight:
                        toAdd = new int[]{R.drawable.blackknight,x,y};
                        list.add(toAdd);
                        break;

                    case R.drawable.blackrook:
                        toAdd = new int[]{R.drawable.blackrook,x,y};
                        list.add(toAdd);
                        break;
                    default:
                        break;
                }
            }
        }
        return list;
    }


    public static void push(PieceCell[][] prevBoard) {
        for(int x = 0; x < 8; x++)
        {
            for(int y = 0; y < 8; y++)
            {
                prevBoard2[x][y][0] = prevBoard[x][y];
            }
        }
    }
}
