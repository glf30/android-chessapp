package chess.cs213;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Jean on 4/24/2018.
 */

public class GameList implements Serializable{
    public static ArrayList<Game> gameList = new ArrayList<>();
    private static final long serialVersionUID = 1L;
    private static Context context;

    public GameList(Context context)
    {
        //this.activity  = activity;
        GameList.context = context;
    }

    public static void add(Game game)
    {
        gameList.add(game);
    }
    public static void remove(Game game)
    {
        gameList.remove(game);
    }

    public static void getFromDAT()
    {
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(context.getFilesDir().getPath().toString() + "/games.dat")))) {
            try {
                gameList = (ArrayList<Game>) ois.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveToDAT()
    {

            Log.i("Test",context.getFilesDir().getPath().toString());

        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(context.getFilesDir().getPath().toString() + "/games.dat")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            oos.writeObject(gameList);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
