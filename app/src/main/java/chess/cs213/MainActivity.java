package chess.cs213;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity{

    public Button playChess;
    public Button replayChess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GameList gameList = new GameList(this);
        GameList.getFromDAT();
        playChess = (Button) findViewById(R.id.buttonPlayChess);
        playChess.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mainIntent = new Intent("chess.cs213.CHESS");
                startActivity(mainIntent);
            }
        });

        replayChess = (Button) findViewById(R.id.buttonReplay);
        replayChess.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mainIntent = new Intent("chess.cs213.REPLAYLIST");
                startActivity(mainIntent);
            }
        });



    }
}
