package chess.cs213;


import java.io.Serializable;

/**
 * Created by Jean on 4/23/2018.
 */

public class Move  implements Serializable {
    public int fromX;
    public int fromY;
    public int toX;
    public int toY;
    private static final long serialVersionUID = 3L;

    public Move(int fromX, int fromY, int toX, int toY) {
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
    }

    @Override
    public String toString()
    {
        return this.fromX + " " + this.fromY + " " + this.toX + " " + this.toY + "\n";
    }
}
