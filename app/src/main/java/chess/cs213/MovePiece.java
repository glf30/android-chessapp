package chess.cs213;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

import chess.pieces.Bishop;
import chess.pieces.BlackPawn;
import chess.pieces.King;
import chess.pieces.Knight;
import chess.pieces.Queen;
import chess.pieces.Rook;
import chess.pieces.WhitePawn;

/**
 * Created by Jean on 4/21/2018.
 */

public class MovePiece extends AppCompatActivity implements View.OnClickListener {

    Context context;
    Activity activity;
    GameBoard board;
    private boolean whiteInCheck = false;
    private boolean blackInCheck = false;
    private boolean blackInCheckmate = false;
    private boolean whiteInCheckmate = false;
    public Game gameToRecord;

    public MovePiece(Context context, Activity activity, Game gameToRecord)
    {
        this.context = context;
        this.activity = activity;
        board = new GameBoard(this.context,this.activity );
        this.gameToRecord = gameToRecord;
    }

    public MovePiece(Context context, Activity activity)
    {
        this.context = context;
        this.activity = activity;
        board = new GameBoard(this.context,this.activity );
        GameBoard.gameOver = true;

    }

    @Override
    public void onClick(View view) {

        if(view instanceof ImageView && GameBoard.gameOver == false)
        {


            if(!GameBoard.currentPiece.isSelected()) {
                ImageView imageView = (ImageView) view;
                PieceCell pieceCell = (PieceCell) imageView.getTag();
                GameBoard.fromX = pieceCell.getX();
                GameBoard.fromY = pieceCell.getY();
                PieceCell[][] prevBoard = board.makePrevBoard();
                GameBoard.push(prevBoard);
                if (pieceCell.getImageResource() != R.drawable.blank) {
                    board.setBackground();

                    if(GameBoard.whiteMove){
                        if(board.isWhite(pieceCell.getImageResource())) {
                            GameBoard.currentPiece.setPiece(GameBoard.board[GameBoard.fromX][GameBoard.fromY]);
                            GameBoard.currentPiece.setX(GameBoard.fromX);
                            GameBoard.currentPiece.setY(GameBoard.fromY);
                            GameBoard.currentPiece.getPiece().setBackgroundColor(Color.YELLOW);
                            GameBoard.currentPiece.setSelected(true);
                        }
                    } else {
                        if(!board.isWhite(pieceCell.getImageResource())) {
                            GameBoard.currentPiece.setPiece(GameBoard.board[GameBoard.fromX][GameBoard.fromY]);
                            GameBoard.currentPiece.setX(GameBoard.fromX);
                            GameBoard.currentPiece.setY(GameBoard.fromY);
                            GameBoard.currentPiece.getPiece().setBackgroundColor(Color.YELLOW);
                            GameBoard.currentPiece.setSelected(true);
                        }
                    }
                }
            }
            else if(GameBoard.currentPiece.isSelected()) {
                ImageView imageView = (ImageView) view;
                PieceCell pieceCell = (PieceCell) imageView.getTag();
                int toX = pieceCell.getX();
                int toY = pieceCell.getY();
                if(GameBoard.whiteMove == true && ((!board.isWhite(pieceCell.getImageResource()) || pieceCell.getImageResource() == R.drawable.blank)))
                {

                    PieceCell currentCell = (PieceCell) GameBoard.currentPiece.getPiece().getTag();
                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blank);
                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blank));

                    GameBoard.blackEPOnWhite = false;
                    switch (currentCell.getImageResource()) {
                        case R.drawable.whitequeen:
                            if (Queen.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.whitequeen);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitequeen));
                                if(GameBoard.Check(false) && whiteInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whitequeen);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whitequeen));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }
                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = false;
                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whitequeen);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whitequeen));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                return;
                            }
                            break;
                        case R.drawable.whitebishop:
                            if (Bishop.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.whitebishop);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitebishop));
                                if(GameBoard.Check(false) && whiteInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whitebishop);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whitebishop));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }
                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = false;
                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whitebishop);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whitebishop));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                return;
                            }
                            break;
                        case R.drawable.whiteking:
                            if (King.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.whiteking);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whiteking));
                                if(GameBoard.Check(false) && whiteInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whiteking);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whiteking));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }
                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = false;
                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whiteking);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whiteking));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                return;
                            }
                            break;
                        case R.drawable.whitepawn:

                            if (WhitePawn.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.whitepawn);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitepawn));
                                if(GameBoard.Check(false) && whiteInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whitepawn);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whitepawn));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }
                                if(toY == 7){

                                    GameBoard.board[toX][toY].setImageResource(R.drawable.whitequeen);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitequeen));


                                }


                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = false;

                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whitepawn);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whitepawn));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                return;
                            }
                            break;
                        case R.drawable.whiteknight:
                            if (Knight.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.whiteknight);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whiteknight));
                                if(GameBoard.Check(false) && whiteInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whiteknight);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whiteknight));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }

                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = false;

                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whiteknight);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whiteknight));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                return;
                            }
                            break;
                        case R.drawable.whiterook:
                            if (Rook.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.whiterook);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whiterook));
                                if(GameBoard.Check(false) && whiteInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whiterook);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whiterook));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }
                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = false;
                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.whiterook);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.whiterook));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                return;
                            }
                            break;
                        default:
                            break;
                    }

                    if(GameBoard.Check(true)){

                        blackInCheck = true;
                        if(isCheckmate(true)) {

                            blackInCheckmate = true;
                        }
                    } else {
                        blackInCheck = false;
                    }


                   if(blackInCheckmate){
                    board.whiteWon = true;
                    board.endGame(activity, gameToRecord);
                    }

                    if(blackInCheck && !blackInCheckmate){
                        Toast.makeText(this.context, "Check", Toast.LENGTH_SHORT).show();
                    }

                    if(!blackInCheckmate) {
                        Toast.makeText(this.context, "Black's Move", Toast.LENGTH_SHORT).show();
                    }

                }
                else if(GameBoard.whiteMove == false && ((board.isWhite(pieceCell.getImageResource()) || pieceCell.getImageResource() == R.drawable.blank)))
                {

                    PieceCell currentCell = (PieceCell) GameBoard.currentPiece.getPiece().getTag();
                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blank);
                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blank));

                    GameBoard.whiteEPOnBlack = false;
                    switch (currentCell.getImageResource()) {
                        case R.drawable.blackqueen:
                            if (Queen.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blackqueen);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackqueen));
                                if(GameBoard.Check(true) && blackInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackqueen);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackqueen));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }

                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = true;
                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackqueen);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackqueen));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                return;
                            }
                            break;
                        case R.drawable.blackbishop:
                            if (Bishop.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blackbishop);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackbishop));
                                if(GameBoard.Check(true) && blackInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackbishop);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackbishop));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }

                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = true;
                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackbishop);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackbishop));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                return;
                            }
                            break;
                        case R.drawable.blackking:
                            if (King.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blackking);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackking));
                                if(GameBoard.Check(true) && blackInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackking);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackking));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }

                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = true;
                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackking);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackking));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                return;
                            }
                            break;
                        case R.drawable.blackpawn:
                            if (BlackPawn.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blackpawn);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackpawn));
                                if(GameBoard.Check(true) && blackInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackpawn);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackpawn));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }

                                if(toY == 0){


                                        GameBoard.board[toX][toY].setImageResource(R.drawable.blackqueen);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackqueen));


                                }




                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = true;

                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackpawn);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackpawn));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                return;
                            }
                            break;
                        case R.drawable.blackknight:
                            if (Knight.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blackknight);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackknight));
                                if(GameBoard.Check(true) && blackInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackknight);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackknight));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }
                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = true;
                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackknight);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackknight));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                return;
                            }
                            break;
                        case R.drawable.blackrook:
                            if (Rook.validMovement(GameBoard.fromX, GameBoard.fromY, toX, toY)) {
                                gameToRecord.add(new Move(GameBoard.fromX,GameBoard.fromY,toX,toY));
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blackrook);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackrook));
                                if(GameBoard.Check(true) && blackInCheck == false) {
                                    GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                    GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                    Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackrook);
                                    GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackrook));
                                    GameBoard.currentPiece.setSelected(false);
                                    board.setBackground();
                                    GameBoard.whiteMove = true;
                                    return;
                                }
                                GameBoard.currentPiece.setSelected(false);
                                GameBoard.whiteMove = true;
                            } else {
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setImageResource(R.drawable.blackrook);
                                GameBoard.board[GameBoard.fromX][GameBoard.fromY].setTag(new PieceCell(GameBoard.fromX, GameBoard.fromY, R.drawable.blackrook));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                return;
                            }
                            break;
                        default:
                            break;
                    }


                    if(GameBoard.Check(false)){

                        whiteInCheck = true;
                        if(isCheckmate(false)) {

                            whiteInCheckmate = true;
                        }
                    } else {
                        whiteInCheck = false;
                    }


                    if(whiteInCheckmate){
                        board.blackWon = true;
                        board.endGame(activity, gameToRecord);
                    }

                    if(whiteInCheck && !whiteInCheckmate){
                        Toast.makeText(this.context, "Check", Toast.LENGTH_SHORT).show();
                    }

                    if(!whiteInCheckmate) {
                        Toast.makeText(this.context, "White's Move", Toast.LENGTH_SHORT).show();
                    }

                }
                GameBoard.currentPiece.setSelected(false);
                board.setBackground();

            }
        }
        else if (view instanceof Button && GameBoard.gameOver == false)
        {
            Button button = (Button) view;
            if(button.getId() == R.id.undo2 && GameBoard.whiteMove == false)
            {
                //Toast.makeText(context, "Undo white",Toast.LENGTH_SHORT).show();
                    board.undo(board);
                    board.setBackground();
                    GameBoard.whiteMove = true;
            }
            else if(button.getId() == R.id.undo1 && GameBoard.whiteMove == true)
            {
                //Toast.makeText(context, "Undo black",Toast.LENGTH_SHORT).show();
                    board.undo(board);
                    board.setBackground();
                    GameBoard.whiteMove = false;
            }
            else if(button.getId() == R.id.resign2)
            {
                board.blackWon = true;
                board.endGame(activity, gameToRecord);
                GameBoard.gameOver = true;
            }
            else if(button.getId() == R.id.resign1)
            {
                board.whiteWon = true;
                board.endGame(activity, gameToRecord);
                GameBoard.gameOver = true;
            }
            else if(button.getId() == R.id.ai2  && GameBoard.whiteMove == true)
            {
                randomMove();
                GameBoard.whiteMove = false;
            }
            else if(button.getId() == R.id.ai1  && GameBoard.whiteMove == false)
            {
                randomMove();
                GameBoard.whiteMove = true;
            }
        }
    }

    public void randomMove()
    {
        Random rand = new Random();

        PieceCell[][] prevBoard = board.makePrevBoard();
        GameBoard.push(prevBoard);
        boolean moved = false;
        while(moved == false)
        {


            int fromX = rand.nextInt(8);
            int fromY = rand.nextInt(8);
            int toX = rand.nextInt(8);
            int toY = rand.nextInt(8);
            int randomPromote = rand.nextInt(4);
            //Toast.makeText(this.context, "fromX:" + fromX + " fromY:"+ fromY + " toX:" + toX + " toY:"+ toY, Toast.LENGTH_LONG).show();

            PieceCell pieceCell = (PieceCell) GameBoard.board[fromX][fromY].getTag();
            PieceCell toCell = (PieceCell)GameBoard.board[toX][toY].getTag();
            if(pieceCell.getImageResource() == R.drawable.blank)
            {
                //Toast.makeText(this.context, "Blank found, continuing", Toast.LENGTH_SHORT).show();
                continue;
            }
            if(GameBoard.whiteMove == true && ((!board.isWhite(toCell.getImageResource()) || toCell.getImageResource() == R.drawable.blank))) {
                GameBoard.blackEPOnWhite = false;
                switch(pieceCell.getImageResource())
                {
                    case R.drawable.whitepawn:
                        if (WhitePawn.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.whitepawn);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitepawn));



                            if(toY == 7){

                                GameBoard.board[toX][toY].setImageResource(R.drawable.whitequeen);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitequeen));


                            }




                            GameBoard.whiteMove = false;
                            moved = true;
                            if(GameBoard.Check(false) && whiteInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.whitepawn);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.whitepawn));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.whiterook:
                        if (Rook.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.whiterook);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whiterook));
                            GameBoard.currentPiece.setSelected(false);
                            GameBoard.whiteMove = false;
                            moved = true;
                            if(GameBoard.Check(false) && whiteInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.whiterook);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.whiterook));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.whitebishop:
                        if (Bishop.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.whitebishop);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitebishop));
                            GameBoard.whiteMove = false;
                            moved = true;
                            if(GameBoard.Check(false) && whiteInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.whitebishop);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.whitebishop));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.whiteknight:
                        if (Knight.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.whiteknight);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whiteknight));
                            GameBoard.whiteMove = false;
                            moved = true;
                            if(GameBoard.Check(false) && whiteInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.whiteknight);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.whiteknight));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.whiteking:
                        if (King.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.whiteking);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whiteking));
                            GameBoard.whiteMove = false;
                            moved = true;
                            if(GameBoard.Check(false) && whiteInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.whitepawn);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.whitepawn));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.whitequeen:
                        if (Queen.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.whitequeen);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitequeen));
                            GameBoard.whiteMove = false;
                            moved = true;
                            if(GameBoard.Check(false) && whiteInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.whitequeen);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.whitequeen));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = true;
                                moved = false;
                            }
                        }
                        break;
                    default:
                        break;
                }
                if(GameBoard.Check(true)){

                    blackInCheck = true;
                    if(isCheckmate(true)) {

                        blackInCheckmate = true;
                    }
                } else {
                    blackInCheck = false;
                }


                if(blackInCheckmate){
                    board.whiteWon = true;
                    board.endGame(activity, gameToRecord);
                }

                if(blackInCheck && !blackInCheckmate){
                    Toast.makeText(this.context, "Check", Toast.LENGTH_SHORT).show();
                }


            }
            else if(GameBoard.whiteMove == false  && ((board.isWhite(toCell.getImageResource()) || toCell.getImageResource() == R.drawable.blank)))
            {
                GameBoard.whiteEPOnBlack = false;
                switch(pieceCell.getImageResource())
                {
                    case R.drawable.blackpawn:
                        if (BlackPawn.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.blackpawn);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackpawn));
                            if(toY == 0){

                                GameBoard.board[toX][toY].setImageResource(R.drawable.blackqueen);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackqueen));


                            }
                            GameBoard.whiteMove = true;
                            moved = true;
                            if(GameBoard.Check(true) && blackInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.blackpawn);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blackpawn));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.blackrook:
                        if (Rook.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.blackrook);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackrook));
                            GameBoard.whiteMove = true;
                            moved = true;
                            if(GameBoard.Check(true) && blackInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.blackrook);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blackrook));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.blackbishop:
                        if (Bishop.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.blackbishop);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackbishop));
                            GameBoard.whiteMove = true;
                            moved = true;
                            if(GameBoard.Check(true) && blackInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.blackrook);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blackrook));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.blackknight:
                        if (Knight.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.blackknight);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackknight));
                            GameBoard.whiteMove = true;
                            moved = true;
                            if(GameBoard.Check(true) && blackInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.blackknight);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blackknight));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.blackking:
                        if (King.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.blackking);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackking));
                            GameBoard.whiteMove = true;
                            moved = true;
                            if(GameBoard.Check(true) && blackInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.blackking);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blackking));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                moved = false;
                            }
                        }
                        break;
                    case R.drawable.blackqueen:
                        if (Queen.validMovement(fromX, fromY, toX, toY)) {
                            gameToRecord.add(new Move(fromX,fromY,toX,toY));
                            GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                            GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                            GameBoard.board[toX][toY].setImageResource(R.drawable.blackqueen);
                            GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackqueen));
                            GameBoard.whiteMove = true;
                            moved = true;
                            if(GameBoard.Check(true) && blackInCheck == false) {
                                GameBoard.board[toX][toY].setImageResource(R.drawable.blank);
                                GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blank));
                                Toast.makeText(this.context, "Invalid Move", Toast.LENGTH_SHORT).show();
                                GameBoard.board[fromX][fromY].setImageResource(R.drawable.blackqueen);
                                GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blackqueen));
                                GameBoard.currentPiece.setSelected(false);
                                board.setBackground();
                                GameBoard.whiteMove = false;
                                moved = false;
                            }
                        }
                        break;
                    default:
                        break;
                }

                if(GameBoard.Check(false)){

                    whiteInCheck = true;
                    if(isCheckmate(false)) {

                        whiteInCheckmate = true;
                    }
                } else {
                    whiteInCheck = false;
                }


                if(whiteInCheckmate){
                    board.blackWon = true;
                    board.endGame(activity, gameToRecord);
                }

                if(whiteInCheck && !whiteInCheckmate){
                    Toast.makeText(this.context, "Check", Toast.LENGTH_SHORT).show();
                }



            }
            if(moved == true)
            {
                break;
            }
        }

    }
    public boolean isCheckmate(boolean blackInCheckmateTest) {
        ArrayList<int[]> whitePieceList = GameBoard.getAllWhitePieces();
        ArrayList<int[]> blackPieceList = GameBoard.getAllBlackPieces();
        //int boardT[][] = new int[8][8];
        int tx = 0;
        int ty = 0;
        /*while(ty < 8) {
            while (tx < 8) {
                boardT[tx][ty] = GameBoard.board[tx][ty];
                tx++;
            }
            tx = 0;
            ty++;
        }*/
        //PieceCell[][] board = makePrevBoard();
        PieceCell[][] boardT = GameBoard.makePrevBoard();

        int w = 0;
        int b = 0;
        int fromX = 0;
        int fromY = 0;
        int toX = 0;
        int toY = 0;

        int x = 0;
        int y = 0;
        //Piece tempPiece = new Piece();
        if(blackInCheckmateTest) {

            while(b < blackPieceList.size()) {
                while(y < 8) {
                    while(x < 8) {


                        fromX = blackPieceList.get(b)[1];
                        fromY = blackPieceList.get(b)[2];

                        toX = x;
                        toY = y;

                        PieceCell pieceTag = (PieceCell) GameBoard.board[fromX][fromY].getTag();
                        PieceCell toPiece = (PieceCell) GameBoard.board[toX][toY].getTag();
                        if((board.isWhite(toPiece.getImageResource()) || toPiece.getImageResource() == R.drawable.blank)) {
                            switch (pieceTag.getImageResource()) {
                                case R.drawable.blackqueen:
                                    if (Queen.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.blackqueen);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackqueen));

                                    }
                                    break;
                                case R.drawable.blackbishop:
                                    if (Bishop.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.blackbishop);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackbishop));
                                    }
                                    break;
                                case R.drawable.blackking:
                                    if (King.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.blackking);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackking));
                                    }
                                    break;

                                case R.drawable.blackpawn:
                                    if (BlackPawn.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.blackpawn);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackpawn));
                                    }
                                    break;

                                case R.drawable.blackknight:
                                    if (Knight.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.blackknight);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackknight));
                                    }
                                    break;
                                case R.drawable.blackrook:
                                    if (Rook.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.blackrook);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.blackrook));
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                        //GameBoard.setBackground();


                        if(!GameBoard.Check(true)) {


                            tx = 0;
                            ty = 0;
                            while(ty < 8) {
                                while (tx < 8) {



                                    GameBoard.board[tx][ty].setImageResource(boardT[tx][ty].getImageResource());
                                    GameBoard.board[tx][ty].setTag(boardT[tx][ty]);
                                    tx++;
                                }
                                tx = 0;
                                ty++;
                            }

                            return false;
                        }

                        tx = 0;
                        ty = 0;
                        while(ty < 8) {
                            while (tx < 8) {



                                GameBoard.board[tx][ty].setImageResource(boardT[tx][ty].getImageResource());
                                GameBoard.board[tx][ty].setTag(boardT[tx][ty]);



                                tx++;
                            }
                            tx = 0;
                            ty++;
                        }



                        x++;



                    }
                    x = 0;
                    y++;

                }
                y = 0;
                b++;

            }

            return true;
        }
        w = 0;
        b = 0;
        fromX = 0;
        fromY = 0;
        toX = 0;
        toY = 0;
        //currPiece = 0;
        //last = 0;
        x = 0;
        y = 0;

        if(blackInCheckmateTest == false) {



            while(w < whitePieceList.size()) {
                while(y < 8) {
                    while(x < 8) {


                        fromX = whitePieceList.get(w)[1];
                        fromY = whitePieceList.get(w)[2];

                        toX = x;
                        toY = y;

                        PieceCell pieceTag = (PieceCell) GameBoard.board[fromX][fromY].getTag();
                        PieceCell toPiece = (PieceCell) GameBoard.board[toX][toY].getTag();
                        if((!board.isWhite(toPiece.getImageResource()) || toPiece.getImageResource() == R.drawable.blank)) {
                            switch (pieceTag.getImageResource()) {
                                case R.drawable.whitequeen:
                                    if (Queen.validMovement(fromX, fromY, toX, toY)) {
                                        Log.i("Umm: ","x: " + toX + " y: " + toY);
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.whitequeen);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitequeen));

                                    }
                                    break;
                                case R.drawable.whitebishop:
                                    if (Bishop.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.whitebishop);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitebishop));
                                    }
                                    break;
                                case R.drawable.whiteking:
                                    if (King.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.whiteking);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whiteking));
                                    }
                                    break;

                                case R.drawable.whitepawn:
                                    if (WhitePawn.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.whitepawn);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whitepawn));
                                    }
                                    break;

                                case R.drawable.whiteknight:
                                    if (Knight.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.whiteknight);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whiteknight));
                                    }
                                    break;
                                case R.drawable.whiterook:
                                    if (Rook.validMovement(fromX, fromY, toX, toY)) {
                                        GameBoard.board[fromX][fromY].setImageResource(R.drawable.blank);
                                        GameBoard.board[fromX][fromY].setTag(new PieceCell(fromX, fromY, R.drawable.blank));
                                        GameBoard.board[toX][toY].setImageResource(R.drawable.whiterook);
                                        GameBoard.board[toX][toY].setTag(new PieceCell(toX, toY, R.drawable.whiterook));
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                        //GameBoard.setBackground();

                        if(!GameBoard.Check(false)) {


                            tx = 0;
                            ty = 0;
                            while(ty < 8) {
                                while (tx < 8) {



                                    GameBoard.board[tx][ty].setImageResource(boardT[tx][ty].getImageResource());
                                    GameBoard.board[tx][ty].setTag(boardT[tx][ty]);
                                    tx++;
                                }
                                tx = 0;
                                ty++;
                            }

                            return false;
                        }

                        tx = 0;
                        ty = 0;
                        while(ty < 8) {
                            while (tx < 8) {



                                GameBoard.board[tx][ty].setImageResource(boardT[tx][ty].getImageResource());
                                GameBoard.board[tx][ty].setTag(boardT[tx][ty]);



                                tx++;
                            }
                            tx = 0;
                            ty++;
                        }



                        x++;



                    }
                    x = 0;
                    y++;

                }
                y = 0;
                w++;

            }

            return true;
        }



        return false;
    }

}
