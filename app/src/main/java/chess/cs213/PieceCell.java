package chess.cs213;

/**
 * Created by Jean on 4/20/2018.
 */

public class PieceCell {
    int x;
    int y;
    int imageResource;

    public PieceCell(int x, int y, int imageResource) {
        this.x = x;
        this.y = y;
        this.imageResource = imageResource;
    }

    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }


    public int getImageResource() {
        return imageResource;
    }


    public String toString()
    {
        return x + " " + y + " " + imageResource;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof PieceCell)) {
            return false;
        }

        PieceCell c = (PieceCell) o;

        return c.getImageResource() == getImageResource();
    }
}
