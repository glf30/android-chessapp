package chess.cs213;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Jean on 4/24/2018.
 */

public class ReplayListActivity extends AppCompatActivity {

    public Button menu;
    private ListView listView;
    public Game game;
    private  ArrayAdapter<Game> arrayAdapter;
    CustomListViewAdapterNameAndDate nameAndDateAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replaylist);
        listView = findViewById(R.id.listView);

        nameAndDateAdapter = new CustomListViewAdapterNameAndDate(this, GameList.gameList);

        listView.setAdapter(nameAndDateAdapter);
        registerForContextMenu(listView);
        menu = findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mainIntent = new Intent("chess.cs213.MAIN");
                startActivity(mainIntent);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                game = (Game) parent.getItemAtPosition(position);

                Intent contentIntent = new Intent("chess.cs213.CHESSREPLAY");
                contentIntent.putExtra("GameObject", game);

                startActivity(contentIntent);
            }
        });
        Toast.makeText(this,"Touch to play a game, long touch to delete",Toast.LENGTH_LONG).show();

    }


    public void sortByDate(View view)
    {
        Collections.sort(GameList.gameList, new Comparator<Game>() {
            public int compare(Game t1, Game t2) {
                //int dateCmp = t2.getDate().compareTo(t1.getDate());
                //Log.i("==Albums==", "dateComp: " + dateCmp);
                return t1.getDate().compareTo(t2.getDate());
            }
        });
        nameAndDateAdapter = new CustomListViewAdapterNameAndDate(this, GameList.gameList);

        listView.setAdapter(nameAndDateAdapter);
        nameAndDateAdapter.notifyDataSetChanged();
        return;
    }

    public void sortByName(View view)
    {
        Collections.sort(GameList.gameList, new Comparator<Game>() {
            public int compare(Game t1, Game t2) {
                return t1.getName().compareTo(t2.getName());
            }
        });
        nameAndDateAdapter = new CustomListViewAdapterNameAndDate(this, GameList.gameList);

        listView.setAdapter(nameAndDateAdapter);
        nameAndDateAdapter.notifyDataSetChanged();
        return;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.listView) {
            menu.add("Delete");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if(item.getTitle() == "Delete")
        {

            //Toast.makeText(this,"You clicked delete",Toast.LENGTH_LONG).show();
            nameAndDateAdapter.remove(nameAndDateAdapter.getItem(info.position));
            nameAndDateAdapter.notifyDataSetChanged();
            GameList.saveToDAT();

        }
        return true;
    }
}
