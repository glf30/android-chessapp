package chess.cs213;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class SplashActivity extends Activity {
    private ImageView chessboard;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        startAnimation();


    }

    private void startAnimation() {

        chessboard = findViewById(R.id.chessSplashScreen);
        ObjectAnimator rotateAnimation = ObjectAnimator.ofFloat(chessboard,"rotation", 0f,360f);
        rotateAnimation.setDuration(2000);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(rotateAnimation);
        animatorSet.start();

        Thread logoTimer = new Thread(){
            public void run(){
                try{

                    sleep(5000);
                    Intent mainIntent = new Intent("chess.cs213.MAIN");
                    startActivity(mainIntent);
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
                finally{
                    finish();
                }
            }
        };
        logoTimer.start();


    }


}
