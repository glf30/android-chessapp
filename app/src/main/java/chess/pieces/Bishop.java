package chess.pieces;




import chess.cs213.GameBoard;
import chess.cs213.PieceCell;
import chess.cs213.R;

public class Bishop {

    public static boolean validMovement(int fromX, int fromY, int toX, int toY){

        if(Math.abs(toX - fromX) == Math.abs(toY - fromY)) {
            int difX = toX - fromX;
            int difY = toY - fromY;
            if(difX > 0) {

                if(difY > 0) { //+1 +1

                    while(fromX != toX && fromY != toY){
                        PieceCell pieceCell = (PieceCell)GameBoard.board[fromX+1][fromY+1].getTag();
                        if(pieceCell.getImageResource() == R.drawable.blank
                                || (fromX+1 == toX && fromY +1 == toY)) {
                            fromX++;
                            fromY++;
                        } else {

                            return false;
                        }



                    }

                } else if(difY < 0) { //+1 -1
                    while(fromX != toX && fromY != toY){
                        PieceCell pieceCell = (PieceCell)GameBoard.board[fromX+1][fromY-1].getTag();
                        if(pieceCell.getImageResource() == R.drawable.blank
                                || (fromX+1 == toX && fromY -1 == toY)) {
                            fromX++;
                            fromY--;
                        } else {

                            return false;
                        }



                    }




                }
            } else if(difX < 0) { //-1 +1
                if(difY > 0) {
                    while(fromX != toX && fromY != toY){
                        PieceCell pieceCell = (PieceCell)GameBoard.board[fromX-1][fromY+1].getTag();
                        if(pieceCell.getImageResource() == R.drawable.blank
                                || (fromX-1 == toX && fromY +1 == toY)) {
                            fromX--;
                            fromY++;
                        } else {

                            return false;
                        }



                    }





                } else if(difY<0) { //-1 -1
                    while(fromX != toX && fromY != toY){
                        PieceCell pieceCell = (PieceCell)GameBoard.board[fromX-1][fromY-1].getTag();
                        if(pieceCell.getImageResource() == R.drawable.blank
                                || (fromX-1 == toX && fromY -1 == toY)) {
                            fromX--;
                            fromY--;
                        } else {

                            return false;
                        }



                    }



                }
            }



            return true;
            }

            //System.out.println("Move not allowed");
            return false;
        }

    }



