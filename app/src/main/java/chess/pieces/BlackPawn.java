package chess.pieces;

import chess.cs213.GameBoard;
import chess.cs213.PieceCell;
import chess.cs213.R;

public class BlackPawn {
    public static boolean validMovement(int fromX, int fromY, int toX, int toY) {

        int distY = toY - fromY;
        int distX = Math.abs(toX - fromX); //Should be 0 most of the time, 1 for attacking
        PieceCell pieceCell = (PieceCell) GameBoard.board[toX][toY].getTag();


        if(distY == -2 && fromY == 6 && distX == 0 && pieceCell.getImageResource() == R.drawable.blank )
        {
            PieceCell front = (PieceCell) GameBoard.board[toX][5].getTag();
                if(front.getImageResource() != R.drawable.blank){
                    return false;
                }
            GameBoard.whiteEPOnBlack = true;
            GameBoard.lastEPX = toX;
            GameBoard.lastEPY = 4;
            return true;
        }
        else if(distY == -1 && distX == 0 && pieceCell.getImageResource() == R.drawable.blank)
        {

            return true;
        }
        else if(distY == -1 && distX == 1 && !(pieceCell.getImageResource() == R.drawable.blank))
        {

            return true;
        }
            //else if(Board.piece == 11 && Board.lastDistX == 0 && Board.lastDistY == 2 && fromY == 3)
            else if (GameBoard.blackEPOnWhite == true && GameBoard.lastEPX == toX && GameBoard.lastEPY == toY+1){
                //ERASE PAWN CODE HERE
                PieceCell blank = (PieceCell) GameBoard.board[GameBoard.lastEPX][GameBoard.lastEPY].getTag();

                if(blank.getImageResource() == R.drawable.whitepawn){

                    GameBoard.board[GameBoard.lastEPX][GameBoard.lastEPY].setImageResource(R.drawable.blank);
                    GameBoard.board[GameBoard.lastEPX][GameBoard.lastEPY].setTag(new PieceCell(GameBoard.lastEPX, GameBoard.lastEPY, R.drawable.blank));


                    return true;
                }




            }



        return false;
    }
}
