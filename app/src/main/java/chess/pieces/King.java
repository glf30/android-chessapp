package chess.pieces;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 * 
 */

import chess.cs213.GameBoard;
import chess.cs213.PieceCell;
import chess.cs213.R;

public class King{
	
	public static boolean validMovement(int posX, int posY, int x, int y){
		
		/*	List formatted as
		 * 	[a][1]
		 * 	[b][2]
		 * 	...
		 * 	[h][8]
		 */
		
		if(posX == 4 && posY == 7){
			if(x == 6 && y == 7) {
				PieceCell pieceCell1 = (PieceCell)GameBoard.board[7][7].getTag();
				PieceCell pieceCell2 = (PieceCell)GameBoard.board[6][7].getTag();
				PieceCell pieceCell3 = (PieceCell)GameBoard.board[5][7].getTag();
				if(pieceCell1.getImageResource() == R.drawable.blackrook &&
					pieceCell2.getImageResource() == R.drawable.blank &&
					pieceCell3.getImageResource() == R.drawable.blank) {

					GameBoard.board[7][7].setImageResource(R.drawable.blank);
					GameBoard.board[7][7].setTag(new PieceCell(7,7, R.drawable.blank));

					GameBoard.board[5][7].setImageResource(R.drawable.blackrook);
					GameBoard.board[5][7].setTag(new PieceCell(5,7, R.drawable.blackrook));
					
					return true;
				} 
				
				
				
				
			} else if(x == 2 && y == 7) {
				PieceCell pieceCell1 = (PieceCell)GameBoard.board[0][7].getTag();
				PieceCell pieceCell2 = (PieceCell)GameBoard.board[1][7].getTag();
				PieceCell pieceCell3 = (PieceCell)GameBoard.board[2][7].getTag();
				PieceCell pieceCell4 = (PieceCell)GameBoard.board[3][7].getTag();
				if(pieceCell1.getImageResource() == R.drawable.blackrook &&
					pieceCell2.getImageResource() == R.drawable.blank &&
					pieceCell3.getImageResource() == R.drawable.blank &&
					pieceCell4.getImageResource() == R.drawable.blank) {
					
					GameBoard.board[0][7].setImageResource(R.drawable.blank);
					GameBoard.board[0][7].setTag(new PieceCell(0,7, R.drawable.blank));

					GameBoard.board[3][7].setImageResource(R.drawable.blackrook);
					GameBoard.board[3][7].setTag(new PieceCell(3,7, R.drawable.blackrook));
					
					
					
					return true;
				}
				
				
				
				
			}
			
			
		} else if(posX == 4 && posY == 0) {
			if(x == 6 && y == 0) {
				PieceCell pieceCell1 = (PieceCell)GameBoard.board[7][0].getTag();
				PieceCell pieceCell2 = (PieceCell)GameBoard.board[6][0].getTag();
				PieceCell pieceCell3 = (PieceCell)GameBoard.board[5][0].getTag();
				if(pieceCell1.getImageResource() == R.drawable.whiterook &&
						pieceCell2.getImageResource() == R.drawable.blank &&
						pieceCell3.getImageResource() == R.drawable.blank) {
					
					GameBoard.board[7][0].setImageResource(R.drawable.blank);
					GameBoard.board[7][0].setTag(new PieceCell(7,0, R.drawable.blank));

					GameBoard.board[5][0].setImageResource(R.drawable.whiterook);
					GameBoard.board[5][0].setTag(new PieceCell(5,0, R.drawable.whiterook));
					
					
					return true;
				} 
				
				
				
			} else if(x == 2 && y == 0) {
				PieceCell pieceCell1 = (PieceCell)GameBoard.board[0][0].getTag();
				PieceCell pieceCell2 = (PieceCell)GameBoard.board[1][0].getTag();
				PieceCell pieceCell3 = (PieceCell)GameBoard.board[2][0].getTag();
				PieceCell pieceCell4 = (PieceCell)GameBoard.board[3][0].getTag();
				if(pieceCell1.getImageResource() == R.drawable.whiterook &&
						pieceCell2.getImageResource() == R.drawable.blank &&
						pieceCell3.getImageResource() == R.drawable.blank &&
						pieceCell4.getImageResource() == R.drawable.blank) {
					
					GameBoard.board[0][0].setImageResource(R.drawable.blank);
					GameBoard.board[0][0].setTag(new PieceCell(0,0, R.drawable.blank));

					GameBoard.board[3][0].setImageResource(R.drawable.whiterook);
					GameBoard.board[3][0].setTag(new PieceCell(3,0, R.drawable.whiterook));
					
					
					
					return true;
				}
				
				
				
				
			}
			
			
		}
		
		
		int[][] validMovements =  new int[8][2];
		
		validMovements[0][0] = posX;
		validMovements[0][1] = posY + 1;
		
		validMovements[1][0] = posX;
		validMovements[1][1] = posY - 1;
		
		validMovements[2][0] = posX - 1;
		validMovements[2][1] = posY;
		
		validMovements[3][0] = posX + 1;
		validMovements[3][1] = posY;
		
		validMovements[4][0] = posX + 1;
		validMovements[4][1] = posY + 1;
		
		validMovements[5][0] = posX - 1;
		validMovements[5][1] = posY + 1;
		
		validMovements[6][0] = posX + 1;
		validMovements[6][1] = posY - 1;
		
		validMovements[7][0] = posX - 1;
		validMovements[7][1] = posY - 1;
		
		boolean valid = false;
		
		for(int c = 0; c < 8; c++)
		{
			if(validMovements[c][0] == x && validMovements[c][1] == y )
			{
				valid = true;
				return valid;
			}
		}
		
		
		
		return valid;
	}

	
}