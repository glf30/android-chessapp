package chess.pieces;


import chess.cs213.GameBoard;
import chess.cs213.PieceCell;
import chess.cs213.R;
public class Rook {


    public static boolean validMovement(int fromX, int fromY, int toX, int toY){


        int dif = 0;


        if(toX == fromX) {
            //fromY = toY;
            dif = toY - fromY;
            while(fromY != toY) {

                if(dif > 0) {
                    PieceCell pieceCell = (PieceCell)GameBoard.board[fromX][fromY+1].getTag();
                    if(pieceCell.getImageResource() == R.drawable.blank || fromY+1 == toY ) {
                        fromY++;
                    } else {

                        return false;
                    }



                } else {
                    PieceCell pieceCell = (PieceCell)GameBoard.board[fromX][fromY-1].getTag();
                    if(pieceCell.getImageResource() == R.drawable.blank || fromY-1 == toY ) {
                        fromY--;
                    } else {

                        return false;
                    }



                }

            }


            return true;
        } else if(toY == fromY) {

            dif = toX - fromX;
            while(fromX != toX) {

                if(dif > 0) {
                    PieceCell pieceCell = (PieceCell)GameBoard.board[fromX+1][fromY].getTag();
                    if(pieceCell.getImageResource() == R.drawable.blank || fromX +1 == toX) {
                        fromX++;
                    } else {

                        return false;
                    }



                } else {
                    PieceCell pieceCell = (PieceCell)GameBoard.board[fromX-1][fromY].getTag();
                    if(pieceCell.getImageResource() == R.drawable.blank || fromX -1 == toX) {
                        fromX--;
                    } else {

                        return false;
                    }



                }

            }



            return true;
        }


        return false;
    }

}
