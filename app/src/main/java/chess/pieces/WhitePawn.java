package chess.pieces;

import android.util.Log;

import chess.cs213.GameBoard;
import chess.cs213.PieceCell;
import chess.cs213.R;

public class WhitePawn {
    public static boolean validMovement(int fromX, int fromY, int toX, int toY) {

        int distY = toY - fromY;
        int distX = Math.abs(toX - fromX); //Should be 0 most of the time, 1 for attacking
        PieceCell pieceCell = (PieceCell) GameBoard.board[toX][toY].getTag();

            if(distY == 2 && fromY == 1 && distX == 0 && pieceCell.getImageResource() == R.drawable.blank)
            {
                PieceCell front = (PieceCell) GameBoard.board[toX][2].getTag();
                if(front.getImageResource() != R.drawable.blank){
                    return false;
                }
                GameBoard.blackEPOnWhite = true;
                GameBoard.lastEPX = toX;
                GameBoard.lastEPY = 3;
                return true;
            }
            else if(distY == 1 && distX == 0 && pieceCell.getImageResource() == R.drawable.blank)
            {

                return true;
            }
            else if(distY == 1 && distX == 1 &&  !(pieceCell.getImageResource() == R.drawable.blank))
            {


                return true;
            }
            else if(GameBoard.whiteEPOnBlack == true && GameBoard.lastEPX == toX && GameBoard.lastEPY == toY-1) {
                //ERASE PAWN CODE HERE
                PieceCell blank = (PieceCell) GameBoard.board[GameBoard.lastEPX][GameBoard.lastEPY].getTag();


                if (blank.getImageResource() == R.drawable.blackpawn) {

                    GameBoard.board[GameBoard.lastEPX][GameBoard.lastEPY].setImageResource(R.drawable.blank);
                    GameBoard.board[GameBoard.lastEPX][GameBoard.lastEPY].setTag(new PieceCell(GameBoard.lastEPX, GameBoard.lastEPY, R.drawable.blank));


                    return true;
                }
            }

        return false;
    }

}
